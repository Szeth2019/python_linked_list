class node:
    def __init__(self, data=None):
        # data stored
        self.data=data
        self.next=None

class linked_list:
    # constructor
    def __init__(self):
        # head node
        # doesn't contain any data; used as placeholder to point to 1st element in the list
        self.head = node()

    # append to end of the linked list
    def append(self, data):
        new_node = node(data)
        cur = self.head

        while cur.next != None:
            cur = cur.next
        cur.next = new_node

    #get the length of the list
    def length(self):

        length = 0
        cur = self.head
        while cur.next is not None:
            length += 1
            cur = cur.next

        return length


    # print contents of list
    def display(self):
        # array of what is stored in "data" in each node
        data_array = []
        cur = self.head

        while cur.next != None:
            cur = cur.next
            data_array.append(cur.data)
        print(data_array)

    # get index i in the linked list
    def get_index(self, index):
        # check if index is larger than the linked list
        if index >= self.length():
            print("ERROR: Index passed in is out of range.")
            return None
        # if not find the desired index and data located there
        else:
            # index of i set to 0
            i = 0
            cur = self.head
            while True:
                cur = cur.next
                if i == index:
                    return cur.data
                i+=1

    def delete_node(self, index):
        # check if index is larger than the linked list
        if index >= self.length():
            print("ERROR: Index passed in is out of range.")
            return
        # if not, find the desired index and delet outer indentation levele that node
        else:
        # index of i set to 0
            i = 0
            current_node = self.head
            while True:
                prev_node = current_node
                current_node = current_node.next

                if i == index:
                    prev_node.next = current_node.next
                    del current_node
                    return
                i += 1

    # Reverses the linked list
    def reverse_list(self):
        # below will set current to the first node in the list
        # also sets previous and next node to None/NULL
        cur = self.head
        cur = cur.next
        prev = None

        while cur is not None:
            # print(cur.data)
            # set next node
            next = cur.next
            # set the next of current node to previous node
            cur.next = prev
            # set previous to this node
            prev = cur
            # set the current to the next node
            cur = next
            # set head to have the new first node as its next
        cur = self.head
        cur.next = prev




LinkedList = linked_list()

LinkedList.display()

LinkedList.append(1)
LinkedList.append(10)
LinkedList.append(11)
LinkedList.append(19)
LinkedList.append(8)
LinkedList.append(7)

LinkedList.display()

print(LinkedList.get_index(1))
print(LinkedList.get_index(0))
print(LinkedList.get_index(100))
print(LinkedList.get_index(5))

LinkedList.display()

LinkedList.delete_node(0)
LinkedList.display()

LinkedList.delete_node(4)
LinkedList.display()

print()
print("Reversing list")
LinkedList.reverse_list()

LinkedList.display()